# Carousel Component Development Assignment

## Introduction

Welcome to AreaHub's 2024 summer intern frontend development assignment! 🎉

In this task, you are required to implement a Carousel Component using HTML, CSS, and JavaScript. This component should provide a dynamic way to showcase a series of images or content items.

## Recommended Tools

If you plan on using a node framework (e.g. React, svelte, vue, etc.), ensure you have the following installed on your machine:

- Node.js
- Git

For a consistent development environment, we use Nix. Please ensure you have Nix installed on your machine. Follow the installation instructions on the [Nix website](https://nixos.org/download.html).

Once Nix is installed, you can set up the project dependencies by running the following command in the frontend project's directory:

```bash
cd frontend
```

```bash
nix-shell
```

Once the nix-shell has been activated, you can init a new project using `pnpm init` and install any necessary dependencies using `pnpm install`.

## Assignment Details

### Objective

Create a responsive carousel component that includes the following features:

- Arrow buttons for navigation to the previous and next slides.
- Indicators (dots) below the carousel for direct navigation to each slide.
- Smooth transitions between slides.
- Responsiveness to adapt to various screen sizes.

### Additional Information

- Feel free to add any extra features or aesthetic enhancements to make the carousel more interactive or visually appealing.
- Ensure your code is clean, well-organized, and commented where necessary.

## Submission Guidelines

### Git Instructions

1. Once you have access to the GitLab repository, create a new branch for your work. Name this branch using your name and the assignment type in kebab-case (e.g., `john-doe-frontend`).
2. Commit your changes to this branch as you work on the assignment.
3. Once you have completed the assignment, push your branch to the repository.

### Merge Request

- Create a merge request on GitLab from your branch to the main branch.
- Ensure that your merge request title clearly indicates the purpose of the request (e.g., "Implement Carousel Component - John Doe").
- In the merge request description, provide a brief overview of the features and functionalities you have implemented.

### README

- Include a README file with your submission that provides:
  - Instructions on how to set up and run your carousel component.
  - A brief description of the features and any additional enhancements you included.

## Time Estimation and Deadline

- Estimated time to complete this assignment: 4-6 hours.
- This assignment is due exactly one week after you receive access to the GitLab repository. Please plan your time accordingly to ensure you meet the deadline.

## Evaluation Criteria

- Functionality: The carousel should work as described and include all required features.
- Code Quality: Your code should be clean, well-organized, and easy to understand.
- Responsiveness: The carousel should adapt to different screen sizes.
- Creativity: Any additional features or design enhancements will be considered a plus.

Good luck with your assignment! We look forward to seeing your carousel component in action.
