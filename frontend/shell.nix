{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.nodejs
    pkgs.nodePackages_latest.pnpm # This adds pnpm to the environment
  ];

  shellHook = ''
    echo "Node.js version: $(node --version)"
    echo "NPM version: $(npm --version)"
    echo "PNPM version: $(pnpm --version)"
  '';
}
