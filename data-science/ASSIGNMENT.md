# Data-Science Assessment: NWS Severe Storms

## Introduction

Welcome to AreaHub's 2024 summer intern data-science assignment! 🎉

For this assignment, you'll be working with NOAA's severe storm dataset. The dataset contains severe weather events that affected the US. You'll be asked to follow the directions layed out in the [ASSIGNMENT.ipynb](/data-science/ASSIGNMENT.ipynb) notebook and answer the question contained within the document. You'll be evaluated on the quality of your code, the accuracy of your answers, and the overall presentation of your work.

## Submission Guidelines

### Git Instructions

1. Once you have access to the GitLab repository, create a new branch for your work. Name this branch using your name and the assignment type in kebab-case (e.g., `john-doe-data-science`).
2. Commit your changes to this branch as you work on the assignment.
3. Once you have completed the assignment, push your branch to the repository.

### Merge Request

- Create a merge request on GitLab from your branch to the main branch.
- Ensure that your merge request title clearly indicates the purpose of the request (e.g., "Completed Data Science Assignment - John Doe").
- In the merge request description, provide a brief overview of the features and functionalities you have implemented.

## Time Estimation and Deadline

- Estimated time to complete this assignment: 1-3 hours.
- The assignment is due exactly one week after you receive access to the GitLab repository.
