# Backend Intern Assessment: TypeScript and NestJS

## Introduction

Welcome to your Backend Intern Assessment! 🎉

In this task, you will be enhancing a pre-initialized NestJS project by developing a REST API for a simple note-taking application. This application will allow users to create, read, update, and delete notes.

## Objective

Your task is to expand the existing NestJS project by adding new features to the note-taking application. Each note should have a title and a description, and the API should support operations to create, read, update, and delete these notes.

## Prerequisites

Before you start, ensure you have the following installed on your machine:

- Node.js
- NestJS CLI
- Git

For a consistent development environment, we use Nix. Please ensure you have Nix installed on your machine. Follow the installation instructions on the [Nix website](https://nixos.org/download.html).

Once Nix is installed, you can set up the project dependencies by running the following command in the backend project's directory:

```bash
cd backend
```

```bash
nix-shell
```

This will install all the necessary dependencies as specified in the `shell.nix` file located in the project's root directory.

## Assignment Details

### API Endpoints

You are required to implement the following endpoints:

1. **Create Note**:

   - Endpoint: `POST /notes`
   - Action: Creates a new note with the provided title and description.

2. **Get All Notes**:

   - Endpoint: `GET /notes`
   - Action: Retrieves all the notes.

3. **Get Note by ID**:

   - Endpoint: `GET /notes/:id`
   - Action: Retrieves a single note by its ID.

4. **Update Note**:

   - Endpoint: `PUT /notes/:id`
   - Action: Updates the title and/or description of the specified note.

5. **Delete Note**:
   - Endpoint: `DELETE /notes/:id`
   - Action: Deletes the specified note.

### Data Storage

- Continue using the in-memory array to store the notes. There's no need to implement a database for this assignment.

## Submission Guidelines

### Git Instructions

1. Create a new branch for your work using your name followed by the assignment type in kebab-case (e.g., `john-doe-backend`).
2. Commit your changes to this branch as you work on the assignment.

### Merge Request

- Once you have completed the assignment, create a merge request on GitLab from your branch to the main branch.
- Title your merge request clearly (e.g., "Enhance Note-taking Application - John Doe") and provide a summary of the changes in the description.

### README

- Update the README file to include:
  - Instructions on how to run the project.
  - A brief overview of the API endpoints you implemented.

## Time Estimation and Deadline

- Estimated time to complete this assignment: 4-6 hours.
- The assignment is due exactly one week after you receive access to the GitLab repository.

## Evaluation Criteria

- Functionality: The API should work as described and include all required features.
- Code Quality: Your code should be clean, well-organized, and easy to read.
- Error Handling: The application should handle and return appropriate error messages.

We are excited to see your implementation and how you tackle this challenge. Good luck! 🤞
