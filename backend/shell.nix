{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.nodejs
    pkgs.nodePackages_latest.pnpm # This adds pnpm to the environment
  ];

  shellHook = ''
    pnpm i -g @nestjs/cli
    echo "Node.js version: $(node --version)"
    echo "Installing Dependencies"
    pnpm install
    echo "Dependencies Installed"
    echo "NPM version: $(npm --version)"
    echo "PNPM version: $(pnpm --version)"
    echo "NestJS version: $(nest --version)"
  '';
}
